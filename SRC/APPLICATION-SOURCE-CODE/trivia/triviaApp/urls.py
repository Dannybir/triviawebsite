from django.conf.urls import patterns, url

from triviaApp import views

urlpatterns = [
    # ex: /triviaApp/
    url(r'^$', views.index, name='index'),
    # ex: /triviaApp/settings/
    url(r'^settings/$', views.settings, name='settings'),
    # ex: /triviaApp/question/
    url(r'^question/$', views.question_enumerator, name='question_enumerator'),
    # ex: /triviaApp/question/question_verifier
    url(r'^question/question_verifier/$', views.question_verifier, name='question_verifier'),
     # ex: /triviaApp/question/results
    url(r'^question/results/$', views.results, name='results'),
         # ex: /triviaApp/setsettings
    url(r'^setsettings/$', views.setsettings, name='setsettings'),
    # ex: /triviaApp/pregame
    url(r'^pregame/$', views.pregame, name='pregame'),
]
