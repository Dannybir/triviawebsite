import requests

def getUrl(fileName):

	WikipediaApiUrl = 'https://en.wikipedia.org/w/api.php'

	reqParameters = { 'action' : 'query', 			\
				'titles' : 'Image:' + fileName, \
				'prop' : 'imageinfo', 		\
				'iiprop' : 'url',		\
				'format' : 'json'}

	request = requests.get(WikipediaApiUrl , 		\
		params  = reqParameters, 			\
		headers = {'Accept'	:'application/json'})

	result = request.json()

	return list(result['query']['pages'].values())[0]['imageinfo'][0]['url']