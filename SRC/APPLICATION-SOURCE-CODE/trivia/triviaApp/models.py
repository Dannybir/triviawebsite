from django.db import models
from random import randint
from triviaApp.DBAccess import DBAccess
import random
import sys
import triviaApp.wikiImage


# Score can only go up if a question is answered correctly
# Modifiers can increase score if timer_bound is lower or if lives_org is lower
def update_score(old_score, lives_org, timer_bound):
    # A correct answer gives 250 pts
    new_score = old_score + 250

    # Low amount of lives, lower modifier, higher score
    lives_modifier = lives_org * 12

    # Lower timer_bound, lower modifier, higher score
    timer_modifier = timer_bound * 2

    return new_score - lives_modifier - timer_modifier


class game_configuration:
    def __init__(self, lives, timer_bound):
        self.lives = lives
        self.timer_bound = timer_bound

    def dump_into_session(self, request):
        request.session['lives'] = self.lives
        request.session['timer_bound'] = self.timer_bound
        request.session['score'] = 0


class question:
    def __init__(self, question_desc, question_answers, question_solution, question_image=None):
        self.q_desc = question_desc
        self.q_ans = question_answers
        self.q_sol = question_solution
        self.q_image = question_image


class question_generator:
    # some notes
    # we have a variable orderVariable  in the functions. this order by assure as we get  random rows from the rows we pulled
    # each question contains the right answer,  a question description, and a possible answers array ( that must be different!)
    # in some queries, in order to make sure the different answers are really different, and to save time, we created a view.
    # arrayFunctions is an array of all the releveant questions generator functions.
    questionsOrder = []  # instead of doing  a normal random, that to the human eye seems not very random, we random a permutation on the questions Order

    current = 0

    def __init__(self):
        # Array to help randomize questions
        self.arrayFunctions = [question_generator.WhatStudioProducedMovie, question_generator.whosFlagIsThis,
                               question_generator.largestSizeCountry, question_generator.mostSuccesfullMovie,
                               question_generator.WhatCountryWasAPlayerBorn, question_generator.mostPopulatedCity,
                               question_generator.PlayersWhoPlayedBothLeagues, question_generator.WhoIsManager,
                               question_generator.WhoIsAGoalKeeper, question_generator.WhoIsTheCapital,
                               question_generator.WhatCurrencyInCountry, question_generator.WhatFilmWasDoneByDir,
                               question_generator.mostPopulatedCountry]

        if (question_generator.current == 0):
            question_generator.questionsOrder = list(range(0, len(self.arrayFunctions)))
            random.shuffle(question_generator.questionsOrder)  # random a permutation

    def mostPopulatedCountry():
        db = DBAccess()
        subquery = 'select countryName , populationTotal  from Country where populationTotal>10000000 '
        orderVariable = '  order by md5(CAST((countryID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []
        max = 0

        for row in data:

            question_answers.append(row['countryName'])
            if (max < row['populationTotal']):
                max = row['populationTotal']
                maxCountry = row['countryName']

        quest = question("Which one is the most populated Country from the following countries?", question_answers,
                         maxCountry)
        return quest

    def largestSizeCountry():
        db = DBAccess()
        subquery = 'select distinct countryName , areaTotal from Country where countryName IS NOT NULL and areaTotal > 1000000 '
        orderVariable = '  order by md5(CAST((countryID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []
        max = 0

        for row in data:

            question_answers.append(row['countryName'])
            if (max < row['areaTotal']):
                max = row['areaTotal']
                maxCountry = row['countryName']

        quest = question("From the following countries, Which one has the largest territory?", question_answers, maxCountry)
        return quest

    def mostSuccesfullMovie():
        db = DBAccess()
        subquery = 'select filmName, boxOffice   from Film where boxOffice > 50000000'
        orderVariable = '  order by md5(CAST((filmID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []
        max = 0

        for row in data:

            question_answers.append(row['filmName'])
            if (max < row['boxOffice']):
                max = row['boxOffice']
                maxCountry = row['filmName']

        quest = question("From the following movies, which one earned the most?", question_answers, maxCountry)
        return quest

    def whosFlagIsThis():
        db = DBAccess()
        subquery = 'select countryName , flagImage from Country where flagImage IS NOT NULL and countryName IS NOT NULL'
        orderVariable = '  order by md5(CAST((countryID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []
        count = 0

        for row in data:

            question_answers.append(row['countryName'])
            if (count == 0):
                answer = row['countryName']
                flag = row['flagImage']
            count = count + 1

        quest = question("Which country does this flag belong to?", question_answers, answer,
                         triviaApp.wikiImage.getUrl(flag))
        return quest

    def WhatFilmWasDoneByDir():
        db = DBAccess()
        subquery = "SELECT filmName, directorName  FROM Film GROUP BY directorName"
        orderVariable = ' order by md5(CAST((Film.filmID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []

        count = 0;

        for row in data:
            if (count == 1):
                answer = row['filmName']
                director = row['directorName']
            count = count + 1;
            question_answers.append(row['filmName'])

        quest = question("Which one of the following films was directed by " + director, question_answers, answer)
        return quest

    def WhatCurrencyInCountry():
        orderVariableOne = ' order by md5(CAST((Country.countryID ^ %s) AS CHAR)) limit 1'
        orderVariableThree = ' order by md5(CAST((Country.countryID ^ %s) AS CHAR)) limit 3'
        view = 'CREATE VIEW answerCurrency AS SELECT DISTINCT countryName ,currencyName   FROM  Country where populationTotal > 10000000 AND currencyName IS NOT NULL' + orderVariableOne + ';'
        query = ' (select  * from answerCurrency )	Union (select distinct "NULL" ,  currencyName from Country where currencyName NOT in ( select currencyName from  answerCurrency)' + orderVariableThree + ')'
        db = DBAccess()

        db.runQuery("DROP VIEW IF EXISTS answerCurrency; ")
        db.runQuery(view, (random.randint(0, sys.maxsize),))
        db.runQuery(query, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []

        for row in data:

            name = row['countryName']
            if (name != "NULL"):
                answer = row['currencyName']
                countryQuestion = name

            question_answers.append(row['currencyName'])
        random.shuffle(question_answers)
	
        quest = question("Which currency is used in " + countryQuestion, question_answers, answer)
        return quest

    def mostPopulatedCity():
        db = DBAccess()
        subquery = 'select cityName , cityPopulation from City where cityPopulation > 10000000 and CityName != "Odagaon" '
        orderVariable = '  order by md5(CAST((cityID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []
        max = 0

        for row in data:

            question_answers.append(row['cityName'])
            if (max < row['cityPopulation']):
                max = row['cityPopulation']
                maxCountry = row['cityName']

        quest = question("Which one is the most populated City from the following cities", question_answers, maxCountry)
        return quest

    def WhoIsTheCapital():
        db = DBAccess()
        subquery = "select countryName , capitalCityID , cityName, populationTotal from Country ,City where cityName IS NOT NULL and capitalCityID IS 			NOT NULL and capitalCityID = City.cityID and populationTotal > 10000000"

        orderVariable = ' order by md5(CAST((Country.countryID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []
        count = 0
        for row in data:

            if (count == 1):
                answer = row['cityName']
                countryQuestion = row['countryName']
            count = count + 1;
            question_answers.append(row['cityName'])

        quest = question("What is the Capital of " + countryQuestion, question_answers, answer)
        return quest

    def WhoIsAGoalKeeper():
        db = DBAccess()
        subqueryGoalKeeper = 'select DISTINCT Person.personName as name , positionName   from SoccerPlayer ,Person, SoccerClub where SoccerPlayer.currentClubID = SoccerClub.clubID and SoccerClub.leagueName = "Premier League" and Person.personID = SoccerPlayer.personID and positionName LIKE %s'
        subqueryNoGoalKeeper = 'select DISTINCT Person.personName as name , positionName   from SoccerPlayer ,Person, SoccerClub where SoccerPlayer.currentClubID = SoccerClub.clubID and SoccerClub.leagueName = "Premier League" and Person.personID = SoccerPlayer.personID and positionName NOT LIKE %s'

        orderVariableOne = ' order by md5(CAST((Person.personID ^ %s) AS CHAR)) limit 1'
        orderVariableThree = ' order by md5(CAST((SoccerPlayer.personID ^ %s) AS CHAR)) limit 3'
        db.runQuery(subqueryGoalKeeper + orderVariableOne, ("%" + "GoalKeeper" + "%", random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []

        for row in data:
            answer = row['name']
        question_answers.append(answer)
        db.runQuery(subqueryNoGoalKeeper + orderVariableThree,
                    ("%" + "GoalKeeper" + "%", random.randint(0, sys.maxsize),))

        data = db.rows

        for row in data:
            question_answers.append(row['name'])

        random.shuffle(question_answers)
        quest = question("Who from the following Soccer players is a Goal-keeper?", question_answers, answer)
        return quest

    def WhatStudioProducedMovie():
        orderVariableOne = ' order by md5(CAST((Film.filmID ^ %s) AS CHAR)) limit 1'
        orderVariableThree = ' order by md5(CAST((Film.filmID ^ %s) AS CHAR)) limit 3'
        view = 'CREATE VIEW answerDistributorName AS select distinct filmName as name,  distributorName  from Film where distributorName IS NOT NULL  and filmBudget > 100000000' + orderVariableOne + ';'
        query = ' (select  * from answerDistributorName )	Union (select "NULL" ,  distributorName from Film where distributorName NOT in ( select distributorName from  answerDistributorName)' + orderVariableThree + ')'
        db = DBAccess()

        db.runQuery("DROP VIEW IF EXISTS answerDistributorName; ")
        db.runQuery(view, (random.randint(0, sys.maxsize),))
        db.runQuery(query, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []

        for row in data:
            name = row['name']
            if (name != "NULL"):
                answer = row['distributorName']
                countryQuestion = name
            question_answers.append(row['distributorName'])
	
	
	
        quest = question("What studio produced the movie  " + countryQuestion, question_answers, answer)
        return quest

    def WhatCountryWasAPlayerBorn():
        orderVariableOne = ' order by md5(CAST((Person.personID ^ %s) AS CHAR)) limit 1'
        orderVariableThree = ' order by md5(CAST((Country.countryID ^ %s) AS CHAR)) limit 3'
        view = 'CREATE VIEW answer AS select Person.personName as name  ,Country.countryName as countryOfBirth from SoccerPlayer ,Person, SoccerClub ,City , Country where SoccerPlayer.currentClubID = SoccerClub.clubID and  (SoccerClub.leagueName = "Premier League" OR SoccerClub.leagueName = "la liga" OR SoccerClub.leagueName = "Serie A" )  and  Person.personID = SoccerPlayer.personID and Person.cityOfBirthID = City.cityID  and City.countryID = Country.countryID' + orderVariableOne + ';'
        query = ' (select  * from answer )	Union (select "NULL" ,  countryName from Country where countryName NOT in ( select countryOfBirth from  answer)' + orderVariableThree + ')'
        db = DBAccess()

        db.runQuery("DROP VIEW IF EXISTS answer; ")
        db.runQuery(view, (random.randint(0, sys.maxsize),))
        db.runQuery(query, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []

        for row in data:

            name = row['name']
            if (name != "NULL"):
                answer = row['countryOfBirth']
                countryQuestion = name

            question_answers.append(row['countryOfBirth'])

        quest = question("What is the birth Country of the Soccer player " + countryQuestion, question_answers, answer)
        return quest

    def WhoIsManager():
        db = DBAccess()
        subquery = 'select clubName, managerName   from  SoccerClub where SoccerClub.leagueName = "Premier League"'

        orderVariable = ' order by md5(CAST((SoccerClub.clubID ^ %s) AS CHAR)) limit 4'
        db.runQuery(subquery + orderVariable, (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []
        count = 0
        for row in data:

            if (count == 1):
                answer = row['managerName']
                countryQuestion = row['clubName']
            count = count + 1;
            question_answers.append(row['managerName'])

        quest = question("Which one of the following is the manager of " + countryQuestion, question_answers, answer)
        return quest

    def PlayersWhoPlayedBothLeagues():
        db = DBAccess()
        orderVariableOne = ' order by md5(CAST((Person.personID ^ %s) AS CHAR)) limit 1'
        orderVariableThree = ' order by md5(CAST((Person.personID ^ %s) AS CHAR)) limit 3'
        # , Person.personID
        playersWhoPlayedSerieA = 'select Distinct Person.personName as name   from SoccerClubSoccerPlayer ,Person, SoccerClub where SoccerClubSoccerPlayer.clubID  = SoccerClub.clubID and Person.personID = SoccerClubSoccerPlayer.personID and SoccerClub.leagueName = "Serie A"'

        playersWhoPlayedPremierLeague = '(select  Person.personID  from SoccerClubSoccerPlayer ,Person, SoccerClub where SoccerClubSoccerPlayer.clubID  = SoccerClub.clubID and Person.personID = SoccerClubSoccerPlayer.personID and SoccerClub.leagueName = "Premier League")'

        playersWhoPlayedLaLiga = '(select  Person.personID  from SoccerClubSoccerPlayer ,Person, SoccerClub where SoccerClubSoccerPlayer.clubID  = SoccerClub.clubID and Person.personID = SoccerClubSoccerPlayer.personID and SoccerClub.leagueName = "la liga")'
        db.runQuery(
            playersWhoPlayedSerieA + "and Person.personID in " + playersWhoPlayedPremierLeague + "and Person.personID in" + playersWhoPlayedLaLiga + orderVariableOne,
            (random.randint(0, sys.maxsize),))

        data = db.rows
        question_answers = []

        for row in data:
            answer = row['name']
        question_answers.append(answer)
        db.runQuery(
            playersWhoPlayedSerieA + "and (Person.personID  NOT in " + playersWhoPlayedPremierLeague + "OR Person.personID NOT in" + playersWhoPlayedLaLiga + ") " + orderVariableThree,
            (random.randint(0, sys.maxsize),))

        data = db.rows

        for row in data:
            question_answers.append(row['name'])

        random.shuffle(question_answers)
        quest = question(
            "Who from the following Soccer players have played in the Premier League, in Serie A and in la liga ?",
            question_answers, answer)
        return quest

    def generate(self):
        # randomly choose a question
        question = self.arrayFunctions[question_generator.questionsOrder[question_generator.current]]()
        question_generator.current = (question_generator.current + 1) % (len(self.arrayFunctions))
	
        return question
