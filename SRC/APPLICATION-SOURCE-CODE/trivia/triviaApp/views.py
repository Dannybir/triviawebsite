from django.shortcuts import render
from django.http import HttpResponse,JsonResponse
from django.http import HttpResponseRedirect
from django.template import Template,Context
from triviaApp.models import *
from triviaApp.DBAccess import DBAccess

# Create your views here.
def index( request ):
	return render(request,'triviaApp/index.html')

def pregame( request ):
	# On first enterance, we need to set the session variables
	if not request.session.get('lives',None):
		request.session['timer_bound_setting'] = 60
		request.session['lives_setting'] = 5

	game = game_configuration(request.session['lives_setting'], request.session['timer_bound_setting'])
	game.dump_into_session(request)
	return HttpResponseRedirect('/triviaApp/question')

def settings( request ):
	return render(request,'triviaApp/settings.html')

def setsettings( request ):
	request.session['timer_bound_setting'] = int(request.POST.get('timer_max'))
	request.session['lives_setting'] = int(request.POST.get('lives'))
	return HttpResponse()

def question_enumerator( request ):
	q_generator = question_generator()
	random_question = q_generator.generate()

	lives = request.session['lives']
	score = request.session['score']
	timer = request.session['timer_bound']
	question_image_href = ""

	# If question has an image, use it
	if random_question.q_image is not None:
		question_image_href = random_question.q_image

	context = {'random_question': random_question , 'lives' : lives, 'score' : score, 'timer_bound' : timer, 'qimage_href' : question_image_href }
	return render(request, 'triviaApp/question.html', context)


def question_verifier( request ):
	user_answer = request.POST.get('user_answer')
	correct_answer = request.POST.get('correct_answer')

	resp_data = {}

	if user_answer == correct_answer:
		# In here we update the score
		score = request.session['score']
		score = update_score( score, request.session['lives'] , request.session['timer_bound'])
		request.session['score'] = score

		# We didn't loose a life
		resp_data['lives'] = 100000;

	else:
		# Update the lives for the session
		lives = request.session['lives']
		request.session['lives'] = lives - 1;
		resp_data['lives'] = lives;

		if( lives <= 0 ):
				resp_data['html_code'] = fetch_leaderboard()
	return JsonResponse(resp_data)


def fetch_leaderboard():
	db = DBAccess();
	db.runQuery("SELECT player_name,player_score FROM triviaApp_leaderboard" )
	leaderboard = db.rows
	leaderboard = sorted(leaderboard, key=lambda x: x['player_score'], reverse=True)
	context = {'leaderboard': leaderboard[:10]  }
	table = Template('''
					<table id>
					    <tr>
					    	<th>Position</th>
					        <th>Player</th>
					        <th>Score</th>
					    </tr>
					    {% for item in leaderboard %}
					    <tr> 
					    	<td>{{ forloop.counter }}</td>
					        <td>{{ item.player_name }}</td>
					        <td>{{ item.player_score }}</td>
					    </tr>
					    {% endfor %}
					</table>
					''')
	return table.render(Context(context))

import hashlib

def make_password(password):
	passw = str(password).encode('utf-8')
	hash = hashlib.md5(passw).hexdigest()
	return hash

def check_password(hash, password):
	"""Generates the hash for a password and compares it."""
	generated_hash = make_password(password)
	return hash == generated_hash

def results( request ):
	# Connect directly to the database
	db = DBAccess();
	user = request.POST.get('user')
	score = request.POST.get('score')
	password = request.POST.get('pass')

	# Hash the password and store it in the database
	password_vald = ""
	
	# Put it in the database
	db.runQuery( "SELECT player_name FROM triviaApp_leaderboard WHERE player_name = %s" , (user,))
	if( db.count == 0 ):
		hashed_passwd = make_password( password )
		db.runQuery( "INSERT INTO triviaApp_leaderboard(player_name,player_score,hashed_pass) VALUES(%s,%s,%s)" , (user,score,hashed_passwd))
	else:
		# Retrieve hashed password of the use
		db.runQuery( "SELECT hashed_pass FROM triviaApp_leaderboard WHERE player_name = %s" , (user,))
		hashed = (db.rows[0])['hashed_pass']
		if not check_password( hashed, password):
			password_vald = "Wrong password, your score is not updated!"
		else: 
			db.runQuery( "UPDATE triviaApp_leaderboard SET player_score = %s WHERE player_name = %s" , (score,user))
			
	# Get the entire table
	db.runQuery("SELECT player_name,player_score FROM triviaApp_leaderboard" )
	leaderboard = db.rows
	leaderboard = sorted(leaderboard, key=lambda x: x['player_score'], reverse=True)

	context = {'leaderboard': leaderboard[:10] , 'password_validation' : password_vald }
	return render(request, 'triviaApp/table.html', context)