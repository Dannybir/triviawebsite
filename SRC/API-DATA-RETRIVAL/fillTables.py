from DataExtractor import DataExtractor
from DBAccess import DBAccess

def extractAndWriteData(db, sparqlQuery, DBtableName):

	ex = DataExtractor(sparqlQuery)

	# build column names to datatypes mapping

	db.runQuery("SHOW Fields FROM " + DBtableName)

	columnNameToTypeMapping = {}
	for column in db.rows:
		columnNameToTypeMapping[column['Field']] = column['Type']

	print("")
	print("====== Updating table " + DBtableName + " ======")
	
	while True:   # continue while data keeps comming
		columnNames, data = ex.getDataPortion()
		if len(data) == 0:
			break;
			
		print( "\tRows extracted from DBPedia: " + str(len(data)), end = "\t")
		
		# build the queries for CUD Batch
		queries = []
		for row in data:
			rowFormated = []
			
			# wrap values in quoted for SQL
			for value, columnName in zip(row, columnNames):
				if value == 'NULL':
					rowFormated.append(None)
				elif columnNameToTypeMapping[columnName].startswith("int"):
					rowFormated.append(int(value))
				elif columnNameToTypeMapping[columnName].startswith("bigint"):
					rowFormated.append(int(value))
				elif columnNameToTypeMapping[columnName].startswith("varchar"):
					rowFormated.append(str(value))
				elif columnNameToTypeMapping[columnName].startswith("var"):
					rowFormated.append(str(value))
				elif columnNameToTypeMapping[columnName].startswith("double"):
					rowFormated.append(float(value))
				elif columnNameToTypeMapping[columnName].startswith("float"):
					rowFormated.append(float(value))
				else:
					rowFormated.append(value)
				

			columnCount = len(rowFormated)
			valuePlaceHolders = ','.join(["%s"]*columnCount)
			columns = ','.join(columnNames)

			# build the query
			SQLquery = "insert ignore into " + DBtableName + " (" + columns + ") values (" + valuePlaceHolders + ")"
			#print(SQLquery.encode('ASCII', 'ignore'))

			# add to queries
			queries.append({"query" : SQLquery, "param" : tuple(rowFormated)})
		
		db.runCUDBatch(queries)

		print("Rows added to DB: " + str(db.count))
		
	print("====== Finished ======")
	
CountrySparqlQuery = """select distinct 
?wikiPageID as ?countryID
?country as ?countryResource
sample(?countryName) as ?countryName
sample(?capitalID ) as ?capitalCityID
sample(?areaTotal/1000000) as ?areaTotal 
sample(?populationTotal) as ?populationTotal
sample(?populationDensity) as ?populationDensity
sample(?anthemName) as ?anthemName
sample(?currencyName) as ?currencyName
sample(?flag) as ?flagImage
sample(?gdpPpp) as ?gdpPpp
sample(?gdpPppPerCapita) as ?gdpPppPerCapita
sample(?hdi) as ?hdi
where 
{
    {
        select distinct ?country
        where
        {
            {?city a dbo:City .}
           UNION
            {?city a yago:City108524735 .}
            ?city dbo:country ?country.
            ?country a dbo:Country.
        }
    }

    ?country dbo:wikiPageID ?wikiPageID;
             rdfs:label ?countryName.
    optional { ?country dbo:areaTotal ?areaTotal }
    optional { ?country dbo:populationDensity ?populationDensity }
    optional { ?country dbo:populationTotal ?populationTotal }
    optional { ?country dbo:capital ?capital.
               ?capital dbo:wikiPageID ?capitalID
             }
    optional { ?country dbo:currency ?currency.
               ?currency rdfs:label ?currencyName
               FILTER ( langMatches(lang(?currencyName), "EN") )
             }
    optional { ?country dbo:flag ?flag }
    optional { ?country dbo:language ?language }
    optional { ?country dbo:anthem ?anthem.
               ?anthem rdfs:label ?anthemName
               FILTER ( langMatches(lang(?anthemName), "EN") )
             }
    optional { ?country dbp:gdpPpp ?gdpPpp 
               FILTER ( datatype(?gdpPpp) =  <http://dbpedia.org/datatype/usDollar>)
             }
    optional { ?country dbp:gdpPppPerCapita ?gdpPppPerCapita 
               FILTER ( datatype(?gdpPppPerCapita) =  <http://dbpedia.org/datatype/usDollar>)
             }
    optional { ?country dbp:hdi ?hdi
               FILTER ( isNumeric(?hdi) )
             }
    FILTER ( langMatches(lang(?countryName), "EN") )
} 
group by ?country ?wikiPageID"""

CapitalSparqlQuery = """select
?cityID
sample(?capital) as ?cityResource
sample(?cityLable) as ?cityName
sample(?countryId) as ?countryID
xsd:integer(sample(?population)) as ?cityPopulation
xsd:integer(sample(?area/1000000)) as ?cityArea
sample(?leaderName) as ?leaderName
where 
{
        {
              select distinct ?country
              where
              {
                  {?city a dbo:City .}
                 UNION
                  {?city a yago:City108524735 .}
                  ?city dbo:country ?country.
                  ?country a dbo:Country.
              }
        }

        ?country dbo:wikiPageID ?countryId.

        ?country dbo:capital ?capital.

	?capital dbo:wikiPageID ?cityID.
	?capital rdfs:label ?cityLable.
	optional 
        { 
            ?capital dbo:populationTotal ?population . 
            filter (xsd:integer(?population) < 50000000 )
        }
	optional { ?capital dbo:areaTotal ?area . }
	optional
	{
		?leader a dbo:Person .
		?capital dbo:leaderName ?leader.
		?leader rdfs:label ?leaderName.
	        filter ( langMatches(lang(?leaderName),"EN"))
	}
	
	filter 
	(
		langMatches(lang(?cityLable),"EN")
		&& !contains(LCASE(?cityLable), "metropolitan")
		&& !contains(LCASE(?cityLable), "downtown")
		&& !contains(LCASE(?cityLable), "urban")
		&& !contains(LCASE(?cityLable), "county")
		&& !contains(LCASE(?cityLable), "district")
		&& !contains(LCASE(?cityLable), "region")
		&& !contains(LCASE(?cityLable), "prefecture")
		&& !contains(LCASE(?cityLable), "demographics")
		&& !contains(LCASE(?cityLable), "corporation")
		&& !contains(LCASE(?cityLable), "development")
		&& !contains(LCASE(?cityLable), "list of")
	)
               
} 
group by ?cityID"""

CitySparqlQuery = """select distinct 
?cityID
sample(?city) as ?cityResource
sample(?cityLable) as ?cityName
sample(?countryId) as ?countryID
xsd:integer(sample(?population)) as ?cityPopulation
xsd:integer(sample(?area/1000000)) as ?cityArea
sample(?leaderName) as ?leaderName

where 
{
	{?city a dbo:City} 
   union 
	{?city a yago:City108524735}

	?city dbo:country ?country.
	?country dbo:wikiPageID ?countryId.
	?city dbo:wikiPageID ?cityID.
	?city rdfs:label ?cityLable.
	optional 
        { 
            ?city dbo:populationTotal ?population . 
            filter (xsd:integer(?population) < 50000000 )
        }
	optional { ?city dbo:areaTotal ?area . }
	optional
	{
		?leader a dbo:Person .
		?city dbo:leaderName ?leader.
		?leader rdfs:label ?leaderName.
	        filter ( langMatches(lang(?leaderName),"EN"))
	}
	
	filter 
	(
		langMatches(lang(?cityLable),"EN")
		&& !contains(LCASE(?cityLable), "metropolitan")
		&& !contains(LCASE(?cityLable), "downtown")
		&& !contains(LCASE(?cityLable), "urban")
		&& !contains(LCASE(?cityLable), "county")
		&& !contains(LCASE(?cityLable), "district")
		&& !contains(LCASE(?cityLable), "region")
		&& !contains(LCASE(?cityLable), "prefecture")
		&& !contains(LCASE(?cityLable), "demographics")
		&& !contains(LCASE(?cityLable), "corporation")
		&& !contains(LCASE(?cityLable), "development")
		&& !contains(LCASE(?cityLable), "list of")
	)
} 
group by ?cityID
order by desc (?cityPopulation)"""

FilmSparqlQuery = """select 
?filmID
sample(?film) as ?filmResource
sample(?filmName) as ?filmName
(max(xsd:double(?filmBudget)) as ?filmBudget) 
sample(?filmRuntime/60) as ?filmRuntime
sample(?boxOffice) as ?boxOffice
sample(?directorName) as ?directorName
sample(?produserName) as ?produserName
sample(?musicComposerName) as ?musicComposerName
sample(?distributorName) as ?distributorName
sample(?airDate) as ?airDate
where 
{
	?film a dbo:Film.

	{ ?film dbo:country dbr:United_States. }
  UNION
	{ ?film dbp:country "United States"@en }

	?film dbo:budget ?filmBudget.
	?film rdfs:label ?filmName.
        ?film dbo:wikiPageID ?filmID.
        optional { ?film dbo:runtime ?filmRuntime. }
        optional { ?film dbo:gross ?boxOffice. }
        optional 
        {
             ?film dbo:director ?director.
             ?director rdfs:label ?directorName.
             FILTER ( langMatches(lang(?directorName), "EN") )
        }
        optional 
        {
             ?film dbo:producer ?produser.
             ?produser rdfs:label ?produserName.
             FILTER ( langMatches(lang(?produserName), "EN") )
        }
        optional 
        {
             ?film dbo:musicComposer ?musicComposer.
             ?musicComposer rdfs:label ?musicComposerName.
             FILTER ( langMatches(lang(?musicComposerName), "EN") )
        }
        optional 
        {
             ?film dbo:distributor ?distributor.
             ?distributor rdfs:label ?distributorName.
             FILTER ( langMatches(lang(?distributorName), "EN") )
        }
        optional 
        {
            ?film dbp:date ?airDate.
            FILTER ( langMatches(lang(?airDate), "EN") )
        }

	FILTER ( datatype(?filmBudget) = <http://dbpedia.org/datatype/usDollar>
		&& langMatches(lang(?filmName), "EN")
		&& !contains(LCASE(?filmName), "series")
		&& !contains(LCASE(?filmName), "franchise")
		&& !contains(LCASE(?filmName), "list of")
		&& !contains(LCASE(?filmName), "trilogy")
	)

	FILTER NOT EXISTS {
		?film dct:subject dbc:Film_series
	}
}
group by ?filmID
order by desc(?filmBudget)"""

PersonActorSparqlQuery = """select 
?personID
sample(?actor) as ?personResource
sample(?personName) as ?personName
sample(?birthDate) as ?birthDate
sample(?cityID) as ?cityOfBirthID
sample(?deathDate) as ?deathDate
sample(?spouseName) as ?spouseName
where 
{
	?film a dbo:Film.

	{ ?film dbo:country dbr:United_States. }
  UNION
	{ ?film dbp:country "United States"@en }

	?film dbo:budget ?filmBudget.
	?film rdfs:label ?filmName.

	FILTER ( datatype(?filmBudget) = <http://dbpedia.org/datatype/usDollar>
		&& langMatches(lang(?filmName), "EN")
		&& !contains(LCASE(?filmName), "series")
		&& !contains(LCASE(?filmName), "franchise")
		&& !contains(LCASE(?filmName), "list of")
		&& !contains(LCASE(?filmName), "trilogy")
	)

	FILTER NOT EXISTS {
		?film dct:subject dbc:Film_series
	}

        ?film dbo:starring ?actor.

        { ?actor a dbo:Actor }
  UNION
        { ?actor a yago:Actor109765278}

        ?actor dbo:wikiPageID ?personID.
        ?actor rdfs:label ?personName.

	FILTER ( langMatches(lang(?personName), "EN"))

	optional
        { 
            ?actor dbo:birthPlace ?city. 
            ?city dbo:wikiPageID ?cityID.

	    {?city a dbo:City .}
          UNION
	    {?city a yago:City108524735 .}  
        }

        optional{ ?actor dbo:birthDate ?birthDate. 
                  FILTER ( datatype(?birthDate) = xsd:date)   
                }
        optional{ ?actor dbo:spouse ?spouse. 
                  ?spouse rdfs:label ?spouseName.

	          FILTER ( langMatches(lang(?spouseName), "EN"))
                }
        optional{ ?actor dbo:deathDate ?deathDate. 
                  FILTER ( datatype(?deathDate) = xsd:date)   
                }
}
group by ?personID"""

ActorSparqlQuery = """select distinct
?personID
where 
{
	?film a dbo:Film.

	{ ?film dbo:country dbr:United_States. }
  UNION
	{ ?film dbp:country "United States"@en }

	?film dbo:budget ?filmBudget.
	?film rdfs:label ?filmName.

	FILTER ( datatype(?filmBudget) = <http://dbpedia.org/datatype/usDollar>
		&& langMatches(lang(?filmName), "EN")
		&& !contains(LCASE(?filmName), "series")
		&& !contains(LCASE(?filmName), "franchise")
		&& !contains(LCASE(?filmName), "list of")
		&& !contains(LCASE(?filmName), "trilogy")
	)

	FILTER NOT EXISTS {
		?film dct:subject dbc:Film_series
	}

        ?film dbo:starring ?actor.

        { ?actor a dbo:Actor }
  UNION
        { ?actor a yago:Actor109765278}

        ?actor dbo:wikiPageID ?personID.
}
order by desc(?film)"""

FilmActorSparqlQuery = """select distinct
?filmID
?personID
where 
{
	?film a dbo:Film.

	{ ?film dbo:country dbr:United_States. }
  UNION
	{ ?film dbp:country "United States"@en }

	?film dbo:budget ?filmBudget.
	?film rdfs:label ?filmName.

	FILTER ( datatype(?filmBudget) = <http://dbpedia.org/datatype/usDollar>
		&& langMatches(lang(?filmName), "EN")
		&& !contains(LCASE(?filmName), "series")
		&& !contains(LCASE(?filmName), "franchise")
		&& !contains(LCASE(?filmName), "list of")
		&& !contains(LCASE(?filmName), "trilogy")
	)

	FILTER NOT EXISTS {
		?film dct:subject dbc:Film_series
	}

        ?film dbo:wikiPageID ?filmID.

        ?film dbo:starring ?actor.

        { ?actor a dbo:Actor }
  UNION
        { ?actor a yago:Actor109765278}

        ?actor dbo:wikiPageID ?personID.


}
order by desc(?film)"""

SoccerPlayerPersonSparqlQuery = """select 
?personID
sample(?soccerPlayer) as ?personResource
sample(?personName) as ?personName
sample(?birthDate) as ?birthDate
sample(?cityID) as ?cityOfBirthID
sample(?deathDate) as ?deathDate
sample(?spouseName) as ?spouseName
where 
{
        ?soccerPlayer a dbo:SoccerPlayer.
        ?soccerPlayer dbp:currentclub ?currentClub.
        ?currentClub a dbo:SoccerClub.

        filter(isIRI(?currentClub))

        ?soccerPlayer dbo:wikiPageID ?personID.
        ?currentClub dbo:wikiPageID ?currentClubID.

        ?soccerPlayer rdfs:label ?personName.

	FILTER ( langMatches(lang(?personName), "EN"))

	optional
        { 
            ?soccerPlayer dbo:birthPlace ?city. 
            ?city dbo:wikiPageID ?cityID.

	    {?city a dbo:City .}
          UNION
	    {?city a yago:City108524735 .}  
        }

        optional{ ?soccerPlayer dbo:birthDate ?birthDate. 
                  FILTER ( datatype(?birthDate) = xsd:date)   
                }
        optional{ ?soccerPlayer dbo:spouse ?spouse. 
                  ?spouse rdfs:label ?spouseName.

	          FILTER ( langMatches(lang(?spouseName), "EN"))
                }
        optional{ ?soccerPlayer dbo:deathDate ?deathDate. 
                  FILTER ( datatype(?deathDate) = xsd:date)   
                }
}
group by ?personID"""

SoccerClubSparqlQuery = """select 
?currentClubID as ?clubID
sample(?currentClub) as ?clubResource
sample(?currentClubName) as ?clubName
sample(?chairmanName) as ?chairmanName
sample(?managerName) as ?managerName
sample(?leagueName) as ?leagueName
where 
{
    ?soccerPlayer a dbo:SoccerPlayer.
    ?soccerPlayer dbp:currentclub ?currentClub.
    ?currentClub a dbo:SoccerClub.

    filter(isIRI(?currentClub))

    ?soccerPlayer dbo:wikiPageID ?personID.
    ?currentClub dbo:wikiPageID ?currentClubID.   

    ?currentClub rdfs:label ?currentClubName.  
    FILTER ( langMatches(lang(?currentClubName), "EN") )

    optional 
    {
        {   
            ?currentClub dbo:chairman ?chairman. 
            ?chairman rdfs:label ?chairmanName.
        }
       UNION
        { 
            ?currentClub dbp:chairman ?chairmanName.
        }
        
        FILTER ( langMatches(lang(?chairmanName), "EN") )
    }

    optional 
    {
        {   
            ?currentClub dbo:manager ?manager. 
            ?manager rdfs:label ?managerName.
        }
       UNION
        { 
            ?currentClub dbp:manager ?managerName.
        }

        FILTER ( langMatches(lang(?managerName), "EN") )
    }
    
    optional
    {
        ?currentClub dbo:league ?league.
        ?league rdfs:label ?leagueName.

        FILTER ( langMatches(lang(?leagueName), "EN") )
    }


} 
group by ?currentClubID"""

SoccerPlayerSparqlQuery = """select 
?personID
sample(?currentClubID) as ?currentClubID
sample(?currentNumber) as ?currentNumber
sample(?positionName) as ?positionName
sample(?height) as ?height
where 
{
    ?soccerPlayer a dbo:SoccerPlayer.
    ?soccerPlayer dbp:currentclub ?currentClub.
    ?currentClub a dbo:SoccerClub.

    filter(isIRI(?currentClub))

    ?soccerPlayer dbo:wikiPageID ?personID.
    ?currentClub dbo:wikiPageID ?currentClubID.

    optional
    {
        ?soccerPlayer  dbp:clubnumber ?currentNumber.
        filter(isNumeric(?currentNumber))
    }
    optional 
    { 
        ?soccerPlayer dbo:position ?position.
        ?position rdfs:label ?positionName.
        filter ( langMatches(lang(?positionName),"EN"))
    }
    optional 
    { 
        ?soccerPlayer dbo:height ?height.
        filter ( ?height > 1.0 && ?height < 2.5 )
    } 
     
} 
group by ?personID"""

SoccerClubSoccerPlayerSparqlQuery = """select distinct
?clubID
?personID
where 
{
        ?soccerPlayer a dbo:SoccerPlayer.
        ?soccerPlayer dbp:currentclub ?currentClub.
        ?currentClub a dbo:SoccerClub.

        filter(isIRI(?currentClub))

        ?soccerPlayer dbo:wikiPageID ?personID.


        ?soccerPlayer dbo:team ?club.

        ?soccerPlayer2 a dbo:SoccerPlayer.
        ?soccerPlayer2 dbp:currentclub ?club.
        ?club a dbo:SoccerClub.

        filter(isIRI(?club))

        ?club dbo:wikiPageID ?clubID.

}"""

db = DBAccess()

extractAndWriteData(db, CountrySparqlQuery, "Country")
extractAndWriteData(db, CapitalSparqlQuery, "City")
extractAndWriteData(db, CitySparqlQuery, "City")
extractAndWriteData(db, FilmSparqlQuery, "Film")
extractAndWriteData(db, PersonActorSparqlQuery, "Person")
extractAndWriteData(db, ActorSparqlQuery , "Actor")
extractAndWriteData(db, FilmActorSparqlQuery , "FilmActor")
extractAndWriteData(db, SoccerPlayerPersonSparqlQuery , "Person")
extractAndWriteData(db, SoccerClubSparqlQuery , "SoccerClub")
extractAndWriteData(db, SoccerPlayerSparqlQuery , "SoccerPlayer")
extractAndWriteData(db, SoccerClubSoccerPlayerSparqlQuery , "SoccerClubSoccerPlayer")