import MySQLdb as mdb

class DBAccess:

	def getConnection():
		con = mdb.connect("127.0.0.1", "DbMysql17", "DbMysql17", "DbMysql17", 3305, charset="utf8", use_unicode=True)
		return con
	
	def runQuery(self, query, params = ()):
		con = DBAccess.getConnection()
		cur = con.cursor(mdb.cursors.DictCursor)
		cur.execute(query, params)
		
		self.rows = cur.fetchall()
		self.count = cur.rowcount
		
		con.commit()
		cur.close()
		con.close()
		
	def runCUDBatch(self, queryAndParamList):
	
		self.runQuery("SET autocommit = false;")
	
		con = DBAccess.getConnection()
		cur = con.cursor()
		
		self.count = 0
		try:
			for queryAndParam in queryAndParamList:
				if "param" not in queryAndParam:
					queryAndParam["param"] = ()
				cur.execute(queryAndParam["query"], queryAndParam["param"])
				self.count += cur.rowcount
		
			con.commit()

		except BaseException as e:
			con.rollback()
			self.count = 0

			print(str(e))
		finally:
			cur.close()
			con.close()
	
	def getRows(self):
		return self.rows

	def getCount(self):
		return self.count
		
		
		
		
		
