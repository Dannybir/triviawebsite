import requests
import time

class DataExtractor:

	ResultSetMaxRows = 2000 	# DBPedia restriction on number of rows in query result
	Epsilon = 100
	DelayBetweenIterations = 500
	DBPediaEndPointUrl = "http://dbpedia.org/sparql"

	def __init__(self, query):
		self.query = query
		self.queryOffset = 0
		self.lastIterTime = 0

	def getDataPortion(self):

		while True:

			currentTime = int(round(time.time() * 1000))

			if(currentTime - self.lastIterTime < DataExtractor.DelayBetweenIterations):
				time.sleep((DataExtractor.DelayBetweenIterations - (currentTime - self.lastIterTime))/1000.0)

			limitQueryFooter = 'LIMIT ' + str(DataExtractor.ResultSetMaxRows - DataExtractor.Epsilon) + ' OFFSET ' + str(self.queryOffset)
			portionQuery = self.query + ' ' + limitQueryFooter

			try:
				request = requests.post(DataExtractor.DBPediaEndPointUrl, 	\
					params  = {'query'	: portionQuery }, 		\
					headers = {'Accept'	:'application/json'})
		
			except (requests.ConnectionError, requests.exceptions.Timeout):
				print("There is a network problem, waiting for 30 seconds and then retrying")
				time.sleep(30)
				print("Retrying...")
				continue
			
			result = request.json()
			self.lastIterTime = int(round(time.time() * 1000))
		
			bindings = result["results"]["bindings"]
			columnNames = result["head"]["vars"]

			rowList = []
			for bind in bindings:
				columnList = []
				for columnName in columnNames:
					if columnName in bind:
						columnList.append(bind[columnName]["value"])
					else:
						columnList.append("NULL")
				rowList.append(columnList)
                            
			# print self.queryOffset
			# print "\n"
			# print portionQuery
			# print self.lastIterTime
		
			self.queryOffset = self.queryOffset + len(bindings)
	
			return (columnNames, rowList)

	def getRowsCount(self): 
		
		return self.queryOffset
