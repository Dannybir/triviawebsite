
# ================================================
# DB version: 0.3   (last updated: 04.06.16)
# ================================================

# create Country table

USE DbMysql17;

ALTER DATABASE DbMysql17
    CHARACTER SET utf8
    COLLATE utf8_unicode_ci;

CREATE TABLE Country
(
	countryID INT,
	countryResource VARCHAR(255),
	countryName VARCHAR(255),
	capitalCityID INT,
	areaTotal DOUBLE,
	populationTotal BIGINT,
	populationDensity DOUBLE,
	anthemName VARCHAR(255),
	currencyName VARCHAR(255),
	flagImage VARCHAR(255),
	gdpPpp DOUBLE,
	gdpPppPerCapita DOUBLE,
	hdi FLOAT,
	INDEX(populationTotal),
	PRIMARY KEY (countryID)
);

CREATE TABLE City
(
	cityID INT,
	cityResource VARCHAR(255),
	cityName VARCHAR(255),
	countryID INT,
	cityPopulation INT,
	cityArea INT,
	leaderName VARCHAR(255),
	PRIMARY KEY (cityID)
);

CREATE TABLE Film
(
	filmID INT,
	filmResource VARCHAR(255),
	filmName VARCHAR(255),
	filmBudget DOUBLE,
	filmRuntime DOUBLE,
	boxOffice DOUBLE,
	directorName VARCHAR(255),
	produserName VARCHAR(255),
        musicComposerName VARCHAR(255),
        distributorName VARCHAR(255),
	airDate VARCHAR(255),
	PRIMARY KEY (filmID)
);

CREATE TABLE Person
(
	personID INT,
        personResource VARCHAR(255),
        personName VARCHAR(255),
        birthDate DATE,
        cityOfBirthID INT,
        deathDate DATE,
        spouseName VARCHAR(255),
	PRIMARY KEY (personID)
);

CREATE TABLE Actor
(
	personID INT,
	PRIMARY KEY (personID),
	FOREIGN KEY (personID) REFERENCES Person(personID)
);

CREATE TABLE FilmActor
(
	filmID INT,
	personID INT,
	CONSTRAINT PrimaryKeyFilmActor PRIMARY KEY (filmID, personID),
	FOREIGN KEY (filmID) REFERENCES Film(filmID),
	FOREIGN KEY (personID) REFERENCES Actor(personID)
);

CREATE TABLE SoccerClub
(
	clubID INT,
	clubResource VARCHAR(255),
	clubName VARCHAR(255),
	chairmanName VARCHAR(255),
	managerName VARCHAR(255),
	leagueName VARCHAR(255),
	INDEX(leagueName),
	PRIMARY KEY (clubID)
);

CREATE TABLE SoccerPlayer
(
	personID INT,
	currentClubID INT,
        currentNumber INT,
        positionName VARCHAR(255),
        height DOUBLE,
     	PRIMARY KEY (personID),
	FOREIGN KEY (personID) REFERENCES Person(personID),
	FOREIGN KEY (currentClubID) REFERENCES SoccerClub(clubID)
);

CREATE TABLE SoccerClubSoccerPlayer
(
	clubID INT,
	personID INT,
	CONSTRAINT PrimaryKeyFilmActor PRIMARY KEY (clubID, personID),
	FOREIGN KEY (clubID) REFERENCES SoccerClub(clubID),
	FOREIGN KEY (personID) REFERENCES SoccerPlayer(personID)
);